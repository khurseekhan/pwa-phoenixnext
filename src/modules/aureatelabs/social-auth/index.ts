import { StorefrontModule } from '@vue-storefront/core/lib/modules'
import { afterRegistration } from './hooks/afterRegistration'
import { authLogin } from './store'

export const KEY = 'social-auth'
export const SocialAuthModule: StorefrontModule = function ({ store, router, appConfig }) {
  store.registerModule(KEY, authLogin)
  afterRegistration(appConfig, store)
}
