import { Module } from 'vuex'
import AuthState from '../types/AuthState'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

export const authLogin: Module<AuthState, any> = {
  namespaced: true,
  state: {
    loginUser: ''
  },
  actions,
  getters,
  mutations
}
